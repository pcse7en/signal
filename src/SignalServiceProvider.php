<?php

namespace Qmeyti\Signal;

use Illuminate\Support\ServiceProvider;

class SignalServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Contracts\Http\Kernel $kernel)
    {
        /**
         * Publish files
         */
        $this->publishes([
            __DIR__ . '/Config/signal_auth.php' => config_path('signal_auth.php'),

            __DIR__ . '/public/signal' => public_path('signal'),

            __DIR__ . '/Resources/view' => resource_path('views/signal'),

            __DIR__ . '/Resources/lang' => resource_path('lang'),
        ]);

        $this->loadMigrationsFrom(__DIR__ . '/Database/migrations');

        /**
         * Load routes
         */
        $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');

//        $this->app['router']->get('/', 'Qmeyti\Signal\App\Controllers\Frontend\HomeController@home');

        /**
         * Load views
         */
        $this->loadViewsFrom(__DIR__ . '/Resources/view', 'signal');

        /**
         * Load translations files
         */
        $this->loadTranslationsFrom(__DIR__ . '/Resources/lang', 'signal');

        $kernel->pushMiddleware(\Qmeyti\Signal\App\Middleware\SignalLoadOptionsMiddleware::class);
        $kernel->pushMiddleware(\Qmeyti\Signal\App\Middleware\SignalLoadLanguageMiddleware::class);
        $kernel->pushMiddleware(\Qmeyti\Signal\App\Middleware\SignalLoadPermissionsMiddleware::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Register configs
         */
        $this->mergeConfigFrom(
            __DIR__ . '/Config/signal_auth.php', 'signal_auth'
        );

        /**
         * Add check auth login check middleware
         */

//        $this->app['router']->pushMiddlewareToGroup('web', SignalLoadOptionsMiddleware::class);
//        $this->app['router']->aliasMiddleware('qlauth', QlAuthCheckMiddleware::class);
//        app('router')->aliasMiddleware('alauth', QlAuthCheckMiddleware::class);

    }

}
