<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Comment extends Model
{
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'comments.body' => 10,
            'comments.name' => 7,
        ],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ip', 'body', 'post_id', 'user_id', 'parent_id', 'depth', 'status', 'mobile', 'email', 'name'];

    /**
     * One to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * One to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * Return user comments count
     *
     * @param User $user
     * @return int
     */
    public static function userCommentsCount(User $user)
    {
        return $user->comments()->count();
    }

    /**
     * Get all of the comments's complaints.
     */
    public function complaints()
    {
        return $this->morphMany(Complaint::class, 'complaintable');
    }
}
