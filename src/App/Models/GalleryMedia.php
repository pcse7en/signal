<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryMedia extends Model
{
    protected $fillable = [
        'header1',
        'header2',
        'gallery_id',
        'link',
        'link_type',
    ];

    protected $table = 'gallery_media';

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }
}
