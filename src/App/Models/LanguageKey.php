<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class LanguageKey extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group',
        'key',
    ];

    protected $table = 'language_keys';

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translates()
    {
        return $this->hasMany(Translate::class, 'language_key_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function translate()
    {
        return $this->hasOne(Translate::class, 'language_key_id', 'id');
    }

    /**
     * Get a language terms with translations
     *
     * @param string|null $language
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|LanguageKey[]
     */
    public static function loadLanguageTerms(string $language = null)
    {
        return LanguageKey::with(['translate' => function ($q) use ($language) {
            $q->where('language', $language);
        }])
            ->get();
    }

    /**
     * Get a term and return translation that on selected language
     *
     * @param $term
     * @param $language
     * @return mixed
     */
    public static function findTermTranslates($term, $language)
    {
         return LanguageKey::leftJoin('translates', 'translates.language_key_id', '=', 'language_keys.id')
            ->where(function ($q) use ($term, $language) {

                $q->where(function ($q2) use ($term, $language) {
                    $q2->where('translates.language', $language);
                    $q2->where('translates.translation', $term);
                });

                $q->orWhere('language_keys.key', $term);

            })
            ->get([
                'language_keys.*',
                'translates.language_key_id',
                'translates.translation',
                'translates.id as translate_id',
                'translates.language',
            ]);
    }
}