<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    public $timestamps = false;

    protected $fillable = ['user_id', 'ip', 'device', 'os', 'browser', 'target', 'action','timestamp'];

    /**
     * Get user visited page
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
