<?php

namespace Qmeyti\Signal\App\Models;

use App\User as MainUser;

use Nicolaslopezj\Searchable\SearchableTrait;


class User extends MainUser
{
    use  SearchableTrait;

    protected $searchable = [
        'columns' => [
            'users.name' => 10,
            'users.last_name' => 9,
            'users.id' => 9,
            'users.mobile' => 7,
            'users.email' => 7,
        ]
    ];

    protected $fillableFields = [
        'name',
        'display_name',
        'first_name',
        'last_name',
        'email',
        'mobile',
        'username',
        'nationalcode',
        'group_id',
        'email_verified_at',
        'mobile_verified_at',
        'language',
        'newsletter',
        'banned',
        'allowed_ip',
        'timezone',
        'land',
        'avatar',
        'info',
        'signature',
        'news_num',
        'comm_num',
        'favorites',
        'time_limit',
        'last_login',
    ];

    /**
     * Set table name
     *
     * @var string
     */
    protected $table = 'users';


    /**
     * Singleton instance
     *
     * @var User
     */
    private static $instance;

    /**
     * User constructor.
     */
    public function __construct()
    {
        /**
         * Singleton new instance
         */
        if (!self::$instance) {

            /**
             * Merge parent model $fillable fields with new register fields
             */
            $this->fillable = array_merge($this->fillable, $this->fillableFields);

            self::$instance = $this;
        }
        return self::$instance;
    }

    /**
     * Find user permissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permits()
    {
        return $this->belongsToMany(Permit::class);

    }

    /**
     * one to many relation to meta_users table
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meta_users()
    {
        return $this->hasMany(MetaUser::class);
    }

    /**
     * one to many relation to groups
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * One to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likes()
    {
        return $this->belongsToMany(Post::class, 'likes', 'user_id', 'post_id');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function favorites()
    {
        return $this->belongsToMany(Post::class, 'favorites', 'user_id', 'post_id');
    }

    /**
     * One to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Get all sliders belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sliders()
    {
        return $this->hasMany(Slider::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function newsletter_submits()
    {
        return $this->hasMany(NewsletterSubmit::class)->where('newsletter_submits.is_member', 0);
    }

    /**
     * Many to one relations with statistics
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statistics()
    {
        return $this->hasMany(Statistic::class);
    }

    /**
     * Get user avatar
     *
     * @param $user
     * @param bool $replaceWithDefault
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public static function getAvatar($user, $replaceWithDefault = true)
    {
        $avatar = '';
        if ($user instanceof User) {
            $avatar = $user->avatar;
        } elseif (is_numeric($user) && $user > 0) {
            $user = User::find(intval($user));
            if ($user !== null)
                $avatar = $user->avatar;
        }

        if (!empty($avatar))
            return url($avatar);

        return url('upload/noavatar.png');
    }

    /**
     * Get all of the user's complaints.
     */
    public function complaints()
    {
        return $this->morphMany(Complaint::class, 'complaintable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function verification()
    {
        return $this->hasMany(Verification::class);
    }
}
