<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'active', 'parent', 'desc', 'icon', 'id'
    ];

    /**
     * Many to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function childes()
    {
        $this->hasMany(Category::class, 'parent', 'id');
    }

    public function parent()
    {
        $this->belongsTo(Category::class);
    }
}
