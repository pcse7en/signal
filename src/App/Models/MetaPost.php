<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class MetaPost extends Model
{
    protected $fillable = ['key', 'value', 'post_id'];

    /**
     * clean time stamp on results
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * one to many relation to meta_posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posts()
    {
        return $this->belongsTo(Post::class);
    }
}
