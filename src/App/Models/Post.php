<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Nicolaslopezj\Searchable\SearchableTrait;


class Post extends Model
{
    use SearchableTrait;
    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'posts.title' => 10,
            'posts.excerpt' => 7,
            'posts.body' => 9,
            'posts.seo_title' => 8,
            'posts.meta_description' => 6,
        ],
    ];

    /**
     * All post statuses is exists
     */
    public const PUBLISH = 'publish';
    public const TRASH = 'trash';
    public const DRAFT = 'draft';
    public const FUTURE = 'future';
    public const PENDING = 'pending';
    public const PRIVATE = 'private';
    public const AUTO_DRAFT = 'auto-draft';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'seo_title',
        'excerpt',
        'body',
        'meta_description',
        'meta_keywords',
        'status',
        'user_id',
        'parent',
        'type',
        'attachment',
        'likes',
        'views',
        'comments',
        'allow_comment',
        'allow_like',
        'allow_main',
        'fixed',
        'feature_photo',
        'language',
    ];

    /**
     * One to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Many to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Many to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likes()
    {
        return $this->belongsToMany(User::class, 'likes', 'post_id', 'user_id');
    }

    /**
     * Many to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function favorites()
    {
        return $this->belongsToMany(Favorite::class);
    }

    /**
     * One to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Many to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * one to many relation to posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meta_posts()
    {
        return $this->hasMany(MetaPost::class);
    }

    /**
     * Get all of the post's complaints.
     */
    public function complaints()
    {
        return $this->morphMany(Complaint::class, 'complaintable');
    }

}
