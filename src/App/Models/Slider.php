<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Slider extends Model
{
    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'sliders.title' => 10,
            'sliders.description' => 8,
        ]
    ];

    protected $fillable = [
        'title',
        'description',
        'type',
        'animation',
        'delay',
        'css',
        'user_id',
        'status',
        'parent',
        'language'
    ];

    /**
     * Get all a user sliders
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
