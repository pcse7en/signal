<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class Permit extends Model
{
    protected $fillable = ['permission', 'desc'];

    /**
     * current user group permissions
     *
     * @var array
     */
    private static $groupPermissions = [];

    /**
     * checked if get user permissions from database
     *
     * @var bool
     */
    private static $groupPermissionsInit = false;


    public $timestamps = false;

    /**
     * many to many relations to groups
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }


    /**
     * Get users with selected permit
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Get user`s group permission
     *
     * @param User|null $user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getUserGroupPermissions($user = null)
    {
        $user = $user ?? auth()->user();

        $permits = Group::with('permits')->whereId($user->group_id)->first();

        return $permits->permits;
    }

    /**
     * Get user permissions
     *
     * @param User|null $user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getUserPermissions($user = null)
    {
        $user = $user ?? auth()->user();

        $permits = User::find($user->id)->with('permits')->first();

        return $permits->permits;
    }
}
