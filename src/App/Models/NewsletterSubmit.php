<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsletterSubmit extends Model
{
    protected $table = 'newsletter_submit';

    protected $fillable = ['newsletter_id', 'newsletter_user_id'];
}
