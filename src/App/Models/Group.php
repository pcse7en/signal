<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Group extends Model
{
    protected $fillable = ['group_name', 'group_alias', 'group_icon'];

    public $timestamps = false;

    /**
     * one to many relation to users
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * many to many relations to permits
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permits()
    {
        return $this->belongsToMany(Permit::class);
    }

    /**
     * get a group all permissions
     *
     * @param null $Group
     * @return mixed
     */
    public static function getGroupPermissions($Group = null)
    {
        return Permit::join('group_permits', 'group_permits.permit_id', '=', 'permits.id')
            ->where('group_permits.group_id', intval($Group ?? Auth::user()->group_id))
            ->pluck('permits.permission')
            ->toArray();
    }
}
