<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Complaint extends Model
{
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'complaints.text' => 10,
            'complaints.writer' => 5,
            'complaints.contact' => 7,
        ],
    ];

    protected $fillable = ['text', 'complaintable_id', 'complaintable_type', 'user_id', 'writer', 'contact', 'status'];

    /**
     * Get the owning commentable model.
     */
    public function complaintable()
    {
        return $this->morphTo();
    }

}
