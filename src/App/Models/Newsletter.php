<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $fillable = [
        'channel',
        'title',
        'text',
        'limit',
        'interval',
        'start_at',
        'finish',
        'user_id',
    ];

    /**
     * Get sender of newsletter
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
