<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class MetaUser extends Model
{
    public $timestamps = false;

    protected $fillable = ['user_id', 'key', 'value'];

    /**
     * Get meta's user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
