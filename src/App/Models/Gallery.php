<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['title','description','type'];

    public function gallery_media()
    {
        return $this->hasMany(GalleryMedia::class);
    }
}