<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageQueue extends Model
{
    protected $fillable = [
        'driver',
        'identifier',
        'module',
        'message',
        'params',
        'status',
        'send_after',
        'try',
        'send_mode'
    ];
}
