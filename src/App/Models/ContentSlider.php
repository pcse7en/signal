<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentSlider extends Model
{
    protected $fillable = [
        'slider_id',
        'header1',
        'header2',
        'text',
        'source',
        'link',
    ];

    /**
     * Get all slider of a slide
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function slider()
    {
        return $this->belongsTo(Slider::class);
    }
}
