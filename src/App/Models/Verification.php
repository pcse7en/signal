<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'mode',
        'code',
        'send_count',
        'send_time',
        'try_count',
        'try_time',
        'verify',
    ];

    /**
     * Get meta's user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
