<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public $timestamps = false;

    protected $fillable = ['key', 'value', 'autoload', 'grouped', 'type'];

    /**
     * Add new or update config
     *
     * @param $key
     * @param $value
     * @param string $type
     * @param string $grouped
     * @param bool $autoload
     */
    public static function addOption($key, $value, $type = 'string', $grouped = 'general', $autoload = false)
    {
        if (self::where('key', $key)->count() > 0) {
            self::where('key', $key)
                ->update([
                    'value' => $value,
                    'type' => $type,
                    'grouped' => $grouped,
                    'autoload' => $autoload
                ]);
        } else {
            self::create([
                'key' => $key,
                'value' => $value,
                'type' => $type,
                'grouped' => $grouped,
                'autoload' => $autoload
            ]);
        }
    }
}
