<?php

namespace Qmeyti\Signal\App\Models;


use Illuminate\Database\Eloquent\Model;

class Translate extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'language_key_id',
        'translation',
        'language'
    ];

    protected $table = 'translates';

    public $timestamps = false;

    /**
     * One to many relation to language keys
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function keys()
    {
        return $this->belongsTo(LanguageKey::class, 'language_key_id', 'id');
    }

}