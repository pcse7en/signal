<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tag extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tag'];

    public $timestamps = false;

    protected $table = 'tags';

    /**
     * Many to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    /**
     * generate new tags for post
     *
     * @param array $tags
     * @return int
     */
    public static function insert_tags($tags)
    {
        $tags = array_values($tags);
        $exists = Tag::whereIn('tag', $tags)->pluck('tag')->toArray();
        $new = [];
        $all = [];
        foreach ($tags as $tag) {
            if (!in_array($tag, $exists))
                $new[] = ['tag' => "$tag"];
            $all[] = $tag;
        }

        if (count($new) > 0) {
            Tag::insert($new);
        }

        $newIds = [];
        if (count($all) > 0)
            $newIds = Tag::whereIn('tag', $all)->pluck('id')->toArray();

        return $newIds;
    }

    /**
     * Get a list of posts ids and return all tags
     *
     * @param array $ids
     * @return \Illuminate\Support\Collection
     */
    public static function posts_tags(array $ids)
    {
        return DB::table('post_tag')
            ->whereIn('post_tag.post_id', array_values($ids))
            ->join('tags', 'post_tag.tag_id', '=', 'tags.id')
            ->get([
                'tags.tag',
                'post_tag.tag_id',
                'post_tag.post_id',
            ]);
    }
}
