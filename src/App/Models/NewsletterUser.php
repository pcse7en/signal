<?php

namespace Qmeyti\Signal\App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsletterUser extends Model
{
    protected $table = 'newsletter_users';

    protected $fillable = ['identifier', 'identifier_type'];
}
