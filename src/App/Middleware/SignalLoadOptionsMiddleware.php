<?php

namespace Qmeyti\Signal\App\Middleware;

use Closure;
use Qmeyti\Signal\App\Libs\Option;

class SignalLoadOptionsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        new Option();
        return $next($request);
    }
}
