<?php

namespace Qmeyti\Signal\App\Middleware;

use Closure;
use Qmeyti\Signal\App\Libs\Permission;

class SignalLoadPermissionsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        new Permission();
        return $next($request);
    }
}
