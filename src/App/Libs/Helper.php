<?php
/**
 * ----------------------------
 * | Permissions
 * ----------------------------
 * |
 * |
 * |
 * |
 * |
 */
if (!function_exists('ioCan')) {
    /**
     * Check user has a permission
     *
     * @param $permit
     * @return bool
     */
    function ioCan($permit)
    {
        return \Qmeyti\Signal\App\Libs\Permission::hasPermission($permit);
    }
}


if (!function_exists('ioPermits')) {
    /**
     * Get all user permissions
     *
     * @return array
     */
    function ioPermits()
    {
        return \Qmeyti\Signal\App\Libs\Permission::getUserPermits();
    }
}


if (!function_exists('ioGroup')) {
    /**
     * Get user group id
     *
     * @return int
     */
    function ioGroup()
    {
        return \Qmeyti\Signal\App\Libs\Permission::getUserGroup();
    }
}


/**
 * ----------------------------
 * | Language
 * ----------------------------
 * |
 * |
 * |
 * |
 * |
 */
if (!function_exists('ioAddTerm')) {
    /**
     * Add new term to translations and languages
     * And return translation key
     *
     * @param $term
     * @param string $group
     * @return mixed
     */
    function ioAddTerm($term, $group = '')
    {
        return \Qmeyti\Signal\App\Libs\Language::addTerm($term, $group);
    }
}

if (!function_exists('_e')) {

    /**
     * Get translate by language key id
     *
     * @param $languageKeyId
     * @return string
     */
    function _e($languageKeyId)
    {
        return \Qmeyti\Signal\App\Libs\Language::getTheTranslate($languageKeyId);
    }
}

if (!function_exists('_t')) {

    /**
     * Get translate by term
     *
     * @param $term
     * @return string
     */
    function _t($term)
    {
        return \Qmeyti\Signal\App\Libs\Language::getTermTranslate($term);
    }
}

/**
 * ----------------------------
 * | Option
 * ----------------------------
 * |
 * |
 * |
 * |
 * |
 */

if (!function_exists('ioOption')) {
    /**
     * Get an option value
     *
     * @param $key
     * @param null $default
     * @return mixed
     */
    function ioOption($key, $default = null)
    {
        return \Qmeyti\Signal\App\Libs\Option::getOption($key, $default);
    }
}

if (!function_exists('ioAddOption')) {
    /**
     * Add new option
     *
     * @param $key
     * @param $value
     * @param string $type
     * @param string $grouped
     * @param bool $autoload
     */
    function ioAddOption($key, $value, $type = 'string', $grouped = 'general', $autoload = false)
    {
        \Qmeyti\Signal\App\Models\Option::addOption($key, $value, $type, $grouped, $autoload);
    }
}


/**
 * ----------------------------
 * | Http
 * ----------------------------
 * |
 * |
 * |
 * |
 * |
 */

/**
 * Check request is from type API
 *
 * @return bool
 */
function ioCheckApiRequest()
{
    return request()->is('api/*');
}

/**
 * ----------------------------
 * | General
 * ----------------------------
 * |
 * |
 * |
 * |
 * |
 */

/**
 * Check is string serialized
 *
 * @param $data
 * @return bool
 */
function ioIsSerialized($data)
{
    // if it isn't a string, it isn't serialized
    if (!is_string($data))
        return false;
    $data = trim($data);
    if ('N;' == $data)
        return true;
    if (!preg_match('/^([adObis]):/', $data, $badions))
        return false;
    switch ($badions[1]) {
        case 'a' :
        case 'O' :
        case 's' :
            if (preg_match("/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data))
                return true;
            break;
        case 'b' :
        case 'i' :
        case 'd' :
            if (preg_match("/^{$badions[1]}:[0-9.E-]+;\$/", $data))
                return true;
            break;
    }
    return false;
}
