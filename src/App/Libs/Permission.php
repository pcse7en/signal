<?php

namespace Qmeyti\Signal\App\Libs;

use Qmeyti\Signal\App\Models\Permit;

class Permission
{
    private static $permissions = [];

    public function __construct()
    {
        if (auth()->check()) {

            $groupPermits = Permit::getUserGroupPermissions();

            foreach ($groupPermits as $item) {
                self::$permissions[] = $item->permission;
            }

            $userPermits = Permit::getUserPermissions();

            foreach ($userPermits as $item) {
                self::$permissions[] = $item->permission;
            }

            self::$permissions = array_values(array_unique(self::$permissions));
        }

    }

    /**
     * Get user group
     *
     * @return int
     */
    public static function getUserGroup()
    {
        return auth()->check() ? auth()->user()->group_id : 0;
    }

    /**
     * Get all user permissions
     *
     * @return array
     */
    public static function getUserPermits()
    {
        return self::$permissions;
    }

    /**
     * Check user has permission
     *
     * @param string $permit
     * @return bool
     */
    public static function hasPermission(string $permit)
    {
        return in_array($permit, self::$permissions);
    }

}