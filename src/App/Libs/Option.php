<?php


namespace Qmeyti\Signal\App\Libs;

/**
 * Class Option
 * Get options
 *
 * @package App\Libs
 */
class Option
{
    /**
     * This is global configs storage that load in begin of the program
     *
     * @var array
     */
    private static $globalOptions = [];

    /**
     * This is a temporary storage for save options
     * and use it in part of the program
     *
     * @var array
     */
    private static $temporaryOptions = [];

    /**
     * Option constructor.
     *
     * @param bool $isGlobal `Use config in all over the program or only in one method or part of the program`
     * @param bool $onlyAutoload
     * @param null|string $grouped
     */
    public function __construct($isGlobal = true, $onlyAutoload = true, $grouped = null)
    {
        $options = new \Qmeyti\Signal\App\Models\Option();

        if ($onlyAutoload === true)
            $options->where('autoload', true);

        if ($grouped !== null && is_string($grouped))
            $options->where('grouped', $grouped);

        $results = $options->get();

        $data = [];
        if ($results->count() > 0) {
            foreach ($results as $result) {
                $converterMethod = ('convertTo' . ucfirst($result->type));
                $data[$result->key] = $this->$converterMethod($result->value);
            }
        }

        if ($isGlobal === true)
            static::$globalOptions = $data;
        else
            static::$temporaryOptions = $data;
    }

    /**
     * Get raw option and return string
     *
     * @param string $rawOption
     * @return string
     */
    private function convertToString(string $rawOption): string
    {
        return $rawOption;
    }

    /**
     * Get raw option and return double
     *
     * @param string $rawOption
     * @return boolean
     */
    private function convertToBool(string $rawOption): bool
    {
        return boolval($rawOption);
    }

    /**
     * Get raw option and return double
     *
     * @param string $rawOption
     * @return double
     */
    private function convertToDouble(string $rawOption)
    {
        return doubleval($rawOption);
    }

    /**
     * Get raw option and return number
     *
     * @param string $rawOption
     * @return integer
     */
    private function convertToNumber(string $rawOption): int
    {
        return intval($rawOption);
    }

    /**
     * Get raw option and return array set
     *
     * @param string $rawOption
     * @return array|false
     */
    private function convertToSet(string $rawOption)
    {
        return explode(',', $rawOption);
    }

    /**
     * Get raw option and return array
     *
     * @param string $rawOption
     * @return array
     */
    private function convertToArray(string $rawOption)
    {
        return ioIsSerialized($rawOption) ? unserialize($rawOption) : [];
    }

    /**
     * Get all global option
     *
     * @return array
     */
    public static function getGlobalOptions()
    {
        return static::$globalOptions;
    }

    /**
     * @return array
     */
    public static function getTemporaryOptions()
    {
        return static::$temporaryOptions;
    }

    /**
     * Get an global option
     *
     * @param string $key
     * @param null|mixed $default
     * @return mixed
     */
    public static function getOption(string $key, $default = null)
    {
        if (isset(static::$globalOptions[$key])) {
            return static::$globalOptions[$key];
        }
        return $default;
    }

}