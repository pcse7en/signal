<?php


Route::name('signal.')->middleware('web')->group(function () {

    /**
     * Frontend
     */
    Route::name('frontend.')->namespace('Qmeyti\Signal\App\Controllers\Frontend')->group(function () {

    });

    /**
     * Backend
     */
    Route::name('backend.')->namespace('Qmeyti\Signal\App\Controllers\Backend')->group(function () {

        Route::get('/login', 'Auth\LoginController@create');

        Route::post('/login', 'Auth\LoginController@authenticate');

    });


});
