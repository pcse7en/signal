<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 191)->nullable();
            $table->text('description')->nullable();
            $table->enum('type', ['text', 'video', 'image', 'post'])->default('image')->index();
            $table->string('animation',191)->nullable();
            $table->unsignedInteger('delay');
            $table->text('css')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('parent');
            $table->string('language',3);
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
