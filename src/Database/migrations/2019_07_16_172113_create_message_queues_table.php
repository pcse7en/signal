<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_queues', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('driver', 20);
            $table->string('module', 20);
            $table->string('identifier', 191);
            $table->text('message')->nullable();
            $table->text('params')->nullable();
            $table->enum('status', ['sent', 'fail', 'wait'])->default('wait');
            $table->enum('send_mode', ['group', 'fast'])->default('group');
            $table->unsignedBigInteger('send_after');
            $table->unsignedInteger('try')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_queues');
    }
}
