<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50);//nice_name
            $table->string('display_name', 100)->nullable();
            $table->string('first_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('email', 191)->unique()->nullable();
            $table->string('mobile', 15)->index()->unique()->nullable();
            $table->string('username', 40)->index()->unique()->nullable();
            $table->string('nationalcode', 11)->index()->unique()->nullable();
            $table->string('password', 80);
            $table->unsignedBigInteger('group_id')->default(5)->index();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('mobile_verified_at')->nullable();

            $table->string('language', 3)->nullable();// User Language eg:en, fa, ir, ...
            $table->boolean('banned')->default(false)->index();// User blocked by admin
            $table->string('allowed_ip', 40)->nullable();// Allowed ip for login with it
            $table->string('timezone', 40)->nullable();
            $table->string('land', 100)->nullable();// User country name
            $table->text('avatar')->nullable();
            $table->text('info')->nullable();//About user
            $table->text('signature')->nullable();
            $table->unsignedInteger('news_num')->default(0);// Number of news
            $table->unsignedInteger('comm_num')->default(0);// Number of comments
            $table->unsignedInteger('favorites')->default(0);// Number of favorite posts
            $table->unsignedInteger('time_limit')->default(0);// Limit time to login
            $table->timestamp('last_login')->nullable();// Last login date

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
