<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration
{

    public function up()
    {
        Schema::table('translates', function (Blueprint $table) {
            $table->foreign('language_key_id')->references('id')->on('language_keys')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('menu_items', function (Blueprint $table) {
            $table->foreign('menu_id')->references('id')->on('menus')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('menu_items', function (Blueprint $table) {
            $table->foreign('parent')->references('id')->on('menu_items')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('group_permit', function (Blueprint $table) {
            $table->foreign('permit_id')->references('id')->on('permits')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('group_permit', function (Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('permit_user', function (Blueprint $table) {
            $table->foreign('permit_id')->references('id')->on('permits')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('permit_user', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('verifications', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('meta_users', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('sliders', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('sliders', function (Blueprint $table) {
            $table->foreign('parent')->references('id')->on('sliders')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('content_sliders', function (Blueprint $table) {
            $table->foreign('slider_id')->references('id')->on('sliders')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


        Schema::table('newsletters', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('newsletter_submit', function (Blueprint $table) {

            $table->foreign('newsletter_user_id')->references('id')->on('newsletter_users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('newsletter_id')->references('id')->on('newsletters')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->foreign('parent')->references('id')->on('categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('parent')->references('id')->on('posts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('post_id')->references('id')->on('posts')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('parent_id')->references('id')->on('comments')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('post_tag', function (Blueprint $table) {
            $table->foreign('post_id')->references('id')->on('posts')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tag_id')->references('id')->on('tags')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('meta_posts', function (Blueprint $table) {
            $table->foreign('post_id')->references('id')->on('posts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('category_post', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('post_id')->references('id')->on('posts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('likes', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('post_id')->references('id')->on('posts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('favorites', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('post_id')->references('id')->on('posts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('statistics', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('complaints', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('gallery_media', function (Blueprint $table) {
            $table->foreign('gallery_id')->references('id')->on('galleries')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        //Translations
        Schema::table('groups', function (Blueprint $table) {
            $table->foreign('group_alias')->references('id')->on('language_keys')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('galleries', function (Blueprint $table) {
            $table->foreign('parent')->references('id')->on('galleries')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('permits', function (Blueprint $table) {
            $table->foreign('desc')->references('id')->on('language_keys')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->foreign('title')->references('id')->on('language_keys')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->foreign('desc')->references('id')->on('language_keys')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('menus', function (Blueprint $table) {
            $table->foreign('name')->references('id')->on('language_keys')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('menu_items', function (Blueprint $table) {
            $table->foreign('label')->references('id')->on('language_keys')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


    }

    public function down()
    {
        Schema::table('translates', function (Blueprint $table) {
            $table->dropForeign('translates_language_key_id_foreign');
        });

        Schema::table('menu_items', function (Blueprint $table) {
            $table->dropForeign('menu_items_menu_id_foreign');
        });

        Schema::table('menu_items', function (Blueprint $table) {
            $table->dropForeign('menu_items_parent_foreign');
        });


        Schema::table('group_permit', function (Blueprint $table) {
            $table->dropForeign('group_permit_group_id_foreign');
            $table->dropForeign('group_permit_permit_id_foreign');
        });

        Schema::table('permit_user', function (Blueprint $table) {
            $table->dropForeign('permit_user_user_id_foreign');
            $table->dropForeign('permit_user_permit_id_foreign');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_group_id_foreign');
        });

        Schema::table('meta_users', function (Blueprint $table) {
            $table->dropForeign('meta_users_user_id_foreign');
        });

        Schema::table('sliders', function (Blueprint $table) {
            $table->dropForeign('sliders_user_id_foreign');
        });

        Schema::table('sliders', function (Blueprint $table) {
            $table->dropForeign('sliders_parent_foreign');
        });

        Schema::table('newsletters', function (Blueprint $table) {
            $table->dropForeign('newsletters_user_id_foreign');
        });

        Schema::table('newsletter_submit', function (Blueprint $table) {
            $table->dropForeign('newsletter_submit_newsletter_user_id_foreign');
            $table->dropForeign('newsletter_submit_newsletter_id_foreign');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->dropForeign('categories_parent_foreign');
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign('posts_user_id_foreign');
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign('posts_parent_foreign');
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->dropForeign('comments_user_id_foreign');
            $table->dropForeign('comments_post_id_foreign');
            $table->dropForeign('comments_parent_id_foreign');
        });

        Schema::table('post_tag', function (Blueprint $table) {
            $table->dropForeign('post_tag_post_id_foreign');
            $table->dropForeign('post_tag_tag_id_foreign');
        });

        Schema::table('meta_posts', function (Blueprint $table) {
            $table->dropForeign('meta_posts_post_id_foreign');
        });

        Schema::table('category_post', function (Blueprint $table) {
            $table->dropForeign('category_post_category_id_foreign');
            $table->dropForeign('category_post_post_id_foreign');
        });

        Schema::table('likes', function (Blueprint $table) {
            $table->dropForeign('likes_user_id_foreign');
            $table->dropForeign('likes_post_id_foreign');
        });

        Schema::table('favorites', function (Blueprint $table) {
            $table->dropForeign('favorites_user_id_foreign');
            $table->dropForeign('favorites_post_id_foreign');
        });

        Schema::table('statistics', function (Blueprint $table) {
            $table->dropForeign('statistics_user_id_foreign');
        });

        Schema::table('complaints', function (Blueprint $table) {
            $table->dropForeign('complaints_user_id_foreign');
        });

        Schema::table('gallery_media', function (Blueprint $table) {
            $table->dropForeign('gallery_media_gallery_id_foreign');
        });

        Schema::table('groups', function (Blueprint $table) {
            $table->dropForeign('groups_group_alias_foreign');
        });

        Schema::table('galleries', function (Blueprint $table) {
            $table->dropForeign('galleries_parent_foreign');
        });

        Schema::table('permits', function (Blueprint $table) {
            $table->dropForeign('permits_desc_foreign');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->dropForeign('categories_title_foreign');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->dropForeign('categories_desc_foreign');
        });

        Schema::table('menus', function (Blueprint $table) {
            $table->dropForeign('menus_name_foreign');
        });

        Schema::table('menu_items', function (Blueprint $table) {
            $table->dropForeign('menu_items_label_foreign');
        });
    }
}
