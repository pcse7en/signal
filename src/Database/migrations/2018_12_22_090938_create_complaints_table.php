<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('text');
            $table->unsignedBigInteger('complaintable_id')->nullable();
            $table->string('complaintable_type', 12);//'post' 'comment' 'user'
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('writer', 100)->nullable();
            $table->string('contact', 191)->nullable();
            $table->enum('status', ['pending', 'done', 'track']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
