<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration
{

    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('title');
            $table->unsignedBigInteger('desc')->nullable();
            $table->string('slug', 60)->unique();
            $table->string('icon', 191)->nullable();
            $table->unsignedBigInteger('parent')->nullable()->default(1);
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::drop('categories');
    }
}
