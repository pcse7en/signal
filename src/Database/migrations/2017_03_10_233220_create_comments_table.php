<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommentsTable extends Migration
{

    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('post_id');
            $table->unsignedBigInteger('parent_id')->nullable()->index();
            $table->unsignedInteger('depth')->default(1)->nullable();
            $table->text('body');

            $table->string('mobile', 15)->nullable();
            $table->string('email', 191)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('ip', 40)->nullable();

            $table->string('status', 10)->default('pending');//eg: spam, publish, pending
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('comments');
    }
}
