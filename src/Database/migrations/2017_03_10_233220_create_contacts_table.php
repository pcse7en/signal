<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration
{

    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 191)->nullable();
            $table->string('email', 191)->nullable();
            $table->string('subject', 191);
            $table->string('phone', 16)->nullable();
            $table->text('message');
            $table->string('ip', 40);
            $table->boolean('seen')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('contacts');
    }
}
