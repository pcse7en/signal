<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryPostTable extends Migration {

	public function up()
	{
		Schema::create('category_post', function(Blueprint $table) {
			$table->unsignedBigInteger('category_id');
			$table->unsignedBigInteger('post_id');
		});
	}

	public function down()
	{
		Schema::drop('category_post');
	}
}
