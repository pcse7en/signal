<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedBigInteger('label');
            $table->string('source', 1000)->nullable();
            $table->string('type', 10);//post, page, link, route
            $table->unsignedBigInteger('parent')
                ->default(0);
            $table->unsignedInteger('sort')->default(0);
            $table->string('class', 191)->nullable();
            $table->string('icon', 191)->nullable();
            $table->unsignedBigInteger('menu_id')->unsigned();
            $table->unsignedInteger('depth')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}
