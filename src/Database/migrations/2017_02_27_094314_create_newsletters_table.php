<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewslettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('channel',10);// eg: sms, email, ...
            $table->string('title', 191);
            $table->text('text');
            $table->smallInteger('limit')->unsigned();
            $table->smallInteger('interval')->unsigned();
            $table->timestamp('start_at');
            $table->boolean('finish')->default(false);
            $table->unsignedBigInteger('user_id')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletters');
    }
}
