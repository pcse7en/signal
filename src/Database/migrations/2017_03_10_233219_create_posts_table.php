<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration
{

    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 191)->nullable();
            $table->string('seo_title', 191)->nullable();
            $table->text('excerpt')->nullable();
            $table->text('body')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();

            $table->string('status', 10)
                ->default('publish')->index();//publish,trash,draft,future,pending,private,auto-draft

            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('parent')->index()->nullable();
            $table->string('type', 20)->index();//eg: post, page
            $table->string('attachment')->nullable();//eg: video url
            $table->unsignedBigInteger('likes')->default(0);
            $table->unsignedBigInteger('views')->default(0);
            $table->unsignedBigInteger('comments')->default(0);
            $table->boolean('allow_comment')->default(1);
            $table->boolean('allow_like')->default(1);
            $table->boolean('allow_main')->default(1);// Allow post show in main page
            $table->boolean('fixed')->default(0);// Show post sticky mode
            $table->string('feature_photo', 191)->nullable();
            $table->string('language', 3)->nullable();// User Language eg:en, fa, ir, ...
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('posts');
    }
}
