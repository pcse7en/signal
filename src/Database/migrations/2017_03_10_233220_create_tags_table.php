<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTagsTable extends Migration {

	public function up()
	{
		Schema::create('tags', function(Blueprint $table) {
			$table->bigIncrements('id');
            $table->string('tag', 50)->unique();
            $table->timestamps();
        });
	}

	public function down()
	{
		Schema::drop('tags');
	}
}
