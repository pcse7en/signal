<?php

namespace Qmeyti\Signal\Database\Seeder;

use Illuminate\Database\Seeder;
use Qmeyti\Signal\App\Models\Option;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options = [
            /**
             * ------------------
             * General
             * -----------------
             * |
             * |
             * |
             * |
             */

            /**
             * Default language
             */
            ['key' => 'general_language', 'value' => 'fa', 'autoload' => 1, 'grouped' => 'general', 'type' => 'string'],

            /**
             * Title
             */
            ['key' => 'general_title', 'value' => 'Test title 1', 'autoload' => 1, 'grouped' => 'general', 'type' => 'string'],

            ['key' => 'general_title_2', 'value' => 'Test title 2', 'autoload' => 1, 'grouped' => 'general', 'type' => 'string'],

            /**
             * Seo site description
             */
            ['key' => 'general_description', 'value' => 'Test description for your site', 'autoload' => 1, 'grouped' => 'general', 'type' => 'string'],

            /**
             * Meta keywords
             */
            ['key' => 'general_keywords', 'value' => 'cms,signal', 'autoload' => 1, 'grouped' => 'general', 'type' => 'set'],

            /**
             * Web site logo image
             */
            ['key' => 'general_logo', 'value' => '', 'autoload' => 1, 'grouped' => 'general', 'type' => 'string'],

            /**
             * Footer html or text
             */
            ['key' => 'general_footer', 'value' => '', 'autoload' => 1, 'grouped' => 'general', 'type' => 'string'],

            /**
             * Default timezone
             */
            ['key' => 'general_timezone', 'value' => 'Asia/Tehran', 'autoload' => 1, 'grouped' => 'general', 'type' => 'string'],

            /**
             * Default template
             */
            ['key' => 'general_default_template', 'value' => 'default', 'autoload' => 1, 'grouped' => 'general', 'type' => 'string'],

            /**
             * Fix mode enable
             */
            ['key' => 'general_fix_mode', 'value' => true, 'autoload' => 1, 'grouped' => 'general', 'type' => 'bool'],

            /**
             * Fix mode message
             */
            ['key' => 'general_fix_mode_message', 'value' => '', 'autoload' => 1, 'grouped' => 'general', 'type' => 'string'],

            /**
             * Fix mode start timestamp
             */
            ['key' => 'general_fix_mode_start_time', 'value' => 150000000, 'autoload' => 1, 'grouped' => 'general', 'type' => 'number'],

            /**
             * Remaining time to reopen site
             *
             * Enter seconds
             */
            ['key' => 'general_remaining_until_reopen', 'value' => 86400, 'autoload' => 1, 'grouped' => 'general', 'type' => 'number'],

            /**
             * ------------------
             * Authentication
             * -----------------
             * |
             * |
             * |
             * |
             */
            /**
             * Redirect users after logout.
             *
             * @var string
             */
            ['key' => 'auth_logout_route', 'value' => 'index', 'autoload' => 1, 'grouped' => 'auth', 'type' => 'string'],

            /**
             * Redirect users after login.
             *
             * @var string
             */
            ['key' => 'auth_login_redirect', 'value' => 'dashboard', 'autoload' => 1, 'grouped' => 'auth', 'type' => 'string'],

            /**
             * Redirect users after registration.
             *
             * @var string
             */
            ['key' => 'auth_register_redirect', 'value' => 'dashboard', 'autoload' => 1, 'grouped' => 'auth', 'type' => 'string'],

            /**
             * Redirect users after verification.
             *
             * @var string
             */
            ['key' => 'auth_verification_redirect', 'value' => 'dashboard', 'autoload' => 1, 'grouped' => 'auth', 'type' => 'string'],

            /**
             * Minimum password length
             */
            ['key' => 'auth_password_min_len', 'value' => 6, 'autoload' => 1, 'grouped' => 'auth', 'type' => 'number'],

            /**
             * Check password in login
             * @notice if config was false password field in login will be disable
             */
            ['key' => 'auth_enable_password_check', 'value' => true, 'autoload' => 1, 'grouped' => 'auth', 'type' => 'bool'],

            /**
             * login key type
             * default values is [sms, password]
             * if `sms` is enable then secret key send to mobile and user enter that
             * if `password` is enable user enter own password
             */
            ['key' => 'auth_key_type', 'value' => 'password', 'autoload' => 1, 'grouped' => 'auth', 'type' => 'string'],

            /**
             * when this method is enable
             * password was encrypt
             * and send to server in two steps
             */
            ['key' => 'auth_safe_login', 'value' => false, 'autoload' => 1, 'grouped' => 'auth', 'type' => 'bool'],

            /**
             * Enable verification after register
             *
             * @var bool
             */
            ['key' => 'auth_verification_enable', 'value' => true, 'autoload' => 1, 'grouped' => 'auth', 'type' => 'bool'],

            /**
             * Identifiers driver list
             */
            ['key' => 'auth_drivers', 'value' =>
                serialize([
                    'email' => [
                        /**
                         * need to verification action
                         */
                        'verification' => true,
                        'login' => true,
                        'register' => true,
                        'title' => 'email',
                        'verification_code_life' => 86400
                    ],
                    'username' => [
                        'verification' => false,
                        'login' => true,
                        'register' => true,
                        'title' => 'username',
                        'verification_code_life' => 0
                    ],
                    'mobile' => [
                        'verification' => true,
                        'login' => true,
                        'register' => true,
                        'title' => 'mobile',
                        'verification_code_life' => 1000
                    ],
                    'nationalcode' => [
                        'verification' => false,
                        'login' => true,
                        'register' => true,
                        'title' => 'national code',
                        'verification_code_life' => 0
                    ]
                ])

                , 'autoload' => 1, 'grouped' => 'auth', 'type' => 'array'],

            /**
             * Verify user by
             */
            ['key' => 'auth_user_verification_with', 'value' => 'email', 'autoload' => 1, 'grouped' => 'auth', 'type' => 'string'],

            /**
             * Maximum number of code in every set
             */
            ['key' => 'auth_maximum_number_of_messages_sent_per_set', 'value' => 3, 'autoload' => 1, 'grouped' => 'auth', 'type' => 'number'],

            /**
             * Delay between send codes
             */
            ['key' => 'auth_delay_between_send_two_codes', 'value' => 15, 'autoload' => 1, 'grouped' => 'auth', 'type' => 'number'],

            /**
             * Delay between send set of codes
             */
            ['key' => 'auth_delay_between_send_set_codes', 'value' => 30, 'autoload' => 1, 'grouped' => 'auth', 'type' => 'number'],

            /**
             * Max try to enter wrong code
             * code will expired after the max try
             */
            ['key' => 'auth_max_try_to_enter_code', 'value' => 3, 'autoload' => 1, 'grouped' => 'auth', 'type' => 'number'],


            /**
             * ------------------
             * Users
             * -----------------
             * |
             * |
             * |
             * |
             */

            //Posts

            //Comments

            //Complaints

            //Admin panel

            //Newsletter

            //Permissions

            //Menus

            //Categories

            //Slider

            //Gallery

        ];

        Option::insert($options);
    }
}
